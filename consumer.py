from aiokafka import AIOKafkaConsumer
from aiokafka.errors import KafkaError
import asyncio

loop = asyncio.get_event_loop()


async def consume():
    consumer = AIOKafkaConsumer(
        'events', 'sonarqube',
        loop=loop,
        bootstrap_servers='kafka:9092',
        auto_offset_reset="earliest"
    )
    # Get cluster layout and join group `my-group`
    await consumer.start()
    count = 0
    try:
        # Consume messages
        async for msg in consumer:
            count += 1
            assert msg.topic in ['events', 'sonarqube']
            assert msg.value in [b'Events message', b'Sonarqube message']
            print("consumed: ", msg.topic, msg.partition, msg.offset,
                  msg.key, msg.value, msg.timestamp)
            if count > 1:
                break

    except KafkaError as err:
        print('Kafka consumer error: ' + err)
        raise err

    finally:
        # Will leave consumer group; perform autocommit if enabled.
        await consumer.stop()

loop.run_until_complete(consume())
